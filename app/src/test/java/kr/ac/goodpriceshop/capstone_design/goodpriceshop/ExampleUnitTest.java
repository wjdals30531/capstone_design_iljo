package kr.ac.goodpriceshop.capstone_design.goodpriceshop;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit start, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }
}