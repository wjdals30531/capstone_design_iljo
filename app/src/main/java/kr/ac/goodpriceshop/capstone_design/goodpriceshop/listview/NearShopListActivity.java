package kr.ac.goodpriceshop.capstone_design.goodpriceshop.listview;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Arrays;

import kr.ac.goodpriceshop.capstone_design.goodpriceshop.CommonData;
import kr.ac.goodpriceshop.capstone_design.goodpriceshop.R;
import kr.ac.goodpriceshop.capstone_design.goodpriceshop.fragment.ListFragment;
import kr.ac.goodpriceshop.capstone_design.goodpriceshop.fragment.map.MultiMarkerMapFragment;
import kr.ac.goodpriceshop.capstone_design.goodpriceshop.util.DataHelper;

/**
 * Created by Osy on 2017-12-08.
 */

public class NearShopListActivity extends Activity {
    private LocationManager locationManager;
    private LocationListener locationListener;

    private ArrayList<StoreVO> typeVOs;
    private ArrayList<StoreVO> storeVOs;
    private ArrayList<StoreVO> nearVOs;

    private double myLat = 0;
    private double myLng = 0;
    private String category = "전체";
    private int radius = 0; //m단위
    private ArrayList categorys;
    private ArrayList radiuss;
    private ArrayList radiussString;

    private ListFragment listFragment;
    private MultiMarkerMapFragment multiMarkerMapFragment;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_maplist);

        initSpinner();
        settingGPS();
        initMyLocation();
        initNearVOs();

        multiMarkerMapFragment = MultiMarkerMapFragment.newFragment( nearVOs, new LatLng( myLat, myLng));
        FragmentTransaction fragmentTransaction1 = getFragmentManager().beginTransaction();
        fragmentTransaction1.replace(R.id.map, multiMarkerMapFragment);
        fragmentTransaction1.commit();

        listFragment = ListFragment.newFragment(nearVOs);
        FragmentTransaction fragmentTransaction2 = getFragmentManager().beginTransaction();
        fragmentTransaction2.replace(R.id.list, listFragment);
        fragmentTransaction2.commit();
    }

    private void initSpinner(){
        String type = getIntent().getStringExtra("type");

        if (type.equals("서비스업")){
            storeVOs = ((CommonData)getApplication()).getServiceVOs();
            categorys = new ArrayList<>(Arrays.asList(new String[]{"전체","목욕업","숙박업","세탁업","이미용업","기타서비스업"}));
        }
        else {
            storeVOs = ((CommonData)getApplication()).getFoodVOs();
            categorys = new ArrayList<>(Arrays.asList(new String[]{"전체","한식","일식","중식","양식","기타양식"}));
        }

        radiuss = new ArrayList<>(Arrays.asList(new Integer[]{100,200,300,500,1000,3000,5000,7000,10000,100000}));
        radiussString = new ArrayList<>(Arrays.asList(new String[]{"100m","200m","300m","500m","1km","3km","5km","7km","10km","100km"}));

        Spinner radiusSpinner = (Spinner)findViewById(R.id.radius);
        ArrayAdapter<ArrayList<Integer>> adapter = new ArrayAdapter<ArrayList<Integer>>(this , android.R.layout.simple_spinner_item, radiussString);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        radiusSpinner.setAdapter(adapter);
        radiusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                radius = (int)radiuss.get(position);
                Log.e(this.toString(), "radius : " + radius);
                initNearVOs();
                listFragment.resetVOs(nearVOs);
                multiMarkerMapFragment.resetVOs(nearVOs);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Spinner categorySpinner = (Spinner)findViewById(R.id.category);
        ArrayAdapter<ArrayList<String>> adapter2 = new ArrayAdapter<ArrayList<String>>(this , android.R.layout.simple_spinner_item, categorys);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categorySpinner.setAdapter(adapter2);
        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                category = (String) categorys.get(position);
                initNearVOs();
                listFragment.resetVOs(nearVOs);
                multiMarkerMapFragment.resetVOs(nearVOs);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    private void initNearVOs(){
        if ( !category.equals("전체") ){
            Log.e(this.toString(), "initNearVOs: 실행이 안되나" );
            typeVOs = DataHelper.getVOsOfCategory(storeVOs, category);
        }
        else {
            typeVOs = (ArrayList<StoreVO>) storeVOs.clone();
        }

        nearVOs = DataHelper.getNearVOs(typeVOs, myLat, myLng, radius);
        Log.e(this.toString(), "nearVOs.size(): " + nearVOs.size() );
    }

    private void settingGPS() {
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                myLat = location.getLatitude();
                myLng = location.getLongitude();

                multiMarkerMapFragment.resetMyLatLng( new LatLng(myLat,myLng));
                int beforeVOSize = nearVOs.size();

                initNearVOs();
                int afterVOSize = nearVOs.size();
                if (beforeVOSize != afterVOSize){
                    listFragment.resetVOs(nearVOs);
                    multiMarkerMapFragment.resetVOs(nearVOs);
                }
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };
    }

    @SuppressLint("MissingPermission")
    private void initMyLocation() {
        Location currentLocation;

        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);

        // 수동으로 위치 구하기
        String locationProvider = LocationManager.GPS_PROVIDER;
        currentLocation = locationManager.getLastKnownLocation(locationProvider);
        if (currentLocation != null) {
            myLng = currentLocation.getLongitude();
            myLat = currentLocation.getLatitude();
            Log.d("Main", "longtitude=" + myLng + ", latitude=" + myLat);
        }

        Log.e(this.toString(), "myLat: " + myLat + "myLng: " + myLng );
    }

    private void change(){

    }
}
