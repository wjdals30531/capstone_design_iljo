package kr.ac.goodpriceshop.capstone_design.goodpriceshop.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import kr.ac.goodpriceshop.capstone_design.goodpriceshop.IntroduceActivity;
import kr.ac.goodpriceshop.capstone_design.goodpriceshop.R;
import kr.ac.goodpriceshop.capstone_design.goodpriceshop.listview.MyAdapter;
import kr.ac.goodpriceshop.capstone_design.goodpriceshop.listview.Provider;
import kr.ac.goodpriceshop.capstone_design.goodpriceshop.listview.StoreVO;

/**
 * Created by Osy on 2017-12-11.
 */

public class ListFragment extends Fragment {
    private ArrayList<StoreVO> nearVOs;
    private MyAdapter myAdapter;

    public static ListFragment newFragment(ArrayList<StoreVO> nearVOs){
        ListFragment INSTANCE = new ListFragment();
        INSTANCE.nearVOs = nearVOs;
        return INSTANCE;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Provider provider = new Provider(nearVOs);
        myAdapter = new MyAdapter(provider ,itemClickListener );

        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setAdapter( myAdapter );

        DividerItemDecoration dividerItemDecoration =
                new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL);

        dividerItemDecoration.setDrawable(getActivity().getResources().getDrawable(R.drawable.list_divider));

        recyclerView.setLayoutManager( new LinearLayoutManager( getActivity() ));
        recyclerView.addItemDecoration(dividerItemDecoration);
    }

    private View.OnClickListener itemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
        int position = (Integer)view.getTag();
        StoreVO storeVO = nearVOs.get(position);

        Intent intent = new Intent( getActivity(), IntroduceActivity.class);
        intent.putExtra( "storeVO", storeVO);
        startActivity(intent);
        }
    };

    public void resetVOs(ArrayList<StoreVO> nearVOs){
        this.nearVOs = nearVOs;
        Provider provider = new Provider(nearVOs);

        myAdapter.setProvider(provider);
    }
}
