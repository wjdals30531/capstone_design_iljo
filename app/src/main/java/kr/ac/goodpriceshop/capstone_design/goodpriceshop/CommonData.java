package kr.ac.goodpriceshop.capstone_design.goodpriceshop;

import android.app.Application;
import android.content.res.AssetManager;

import java.util.ArrayList;

import kr.ac.goodpriceshop.capstone_design.goodpriceshop.listview.StoreVO;
import kr.ac.goodpriceshop.capstone_design.goodpriceshop.util.DataHelper;
import kr.ac.goodpriceshop.capstone_design.goodpriceshop.util.JsonUtil;

/**
 * Created by Osy on 2017-11-24.
 */

public class CommonData extends Application {
    private ArrayList<StoreVO> foodVOs;
    private ArrayList<StoreVO> serviceVOs;
    private StoreVO storeVO;
    
    @Override
    public void onCreate() {
        super.onCreate();

        AssetManager assetManager = getResources().getAssets();
        JsonUtil jsonUtil = new JsonUtil(assetManager);
        ArrayList<StoreVO> allVOs = jsonUtil.getStoreVOs();

        DataHelper dataHelper = new DataHelper();
        dataHelper.setVos(allVOs);

        foodVOs = dataHelper.getFoodShop();
        serviceVOs = dataHelper.getServiceShop();
    }
    public ArrayList<StoreVO> getServiceVOs() {
        return serviceVOs;
    }
    public ArrayList<StoreVO> getFoodVOs() {
        return foodVOs;
    }

    public void setStoreVO(StoreVO storeVO) {
        this.storeVO = storeVO;
    }
}
