package kr.ac.goodpriceshop.capstone_design.goodpriceshop.util;

import android.location.Location;

import java.util.ArrayList;

import kr.ac.goodpriceshop.capstone_design.goodpriceshop.listview.StoreVO;

/**
 * Created by Osy on 2017-12-14.
 */

public class DataHelper {
    private ArrayList<StoreVO> vos;

    public void setVos(ArrayList<StoreVO> vos) {
        this.vos = vos;
    }

    public ArrayList<StoreVO> getFoodShop(){
        ArrayList<StoreVO> arrayList = new ArrayList();

        for (StoreVO vo:
             vos) {
            if (vo.getCategory().equals("한식") ||
                vo.getCategory().equals("일식") ||
                vo.getCategory().equals("중식") ||
                vo.getCategory().equals("양식") ||
                vo.getCategory().equals("기타양식")){
                arrayList.add(vo);
            }
        }

        return arrayList;
    }

    public ArrayList<StoreVO> getServiceShop(){
        ArrayList<StoreVO> arrayList = new ArrayList();

        for (StoreVO vo:
             vos) {
            if (vo.getCategory().equals("세탁업") ||
                vo.getCategory().equals("목욕업") ||
                vo.getCategory().equals("숙박업") ||
                vo.getCategory().equals("이미용업") ||
                vo.getCategory().equals("기타서비스")){
                arrayList.add(vo);
            }
        }

        return arrayList;
    }

    public static ArrayList<StoreVO> getVOsOfCategory(ArrayList<StoreVO> vos, String category){
        ArrayList<StoreVO> arrayList = new ArrayList();

        for (StoreVO vo:
                vos) {
            if (vo.getCategory().equals(category)){
                arrayList.add(vo);
            }
        }

        return arrayList;
    }

    public static ArrayList<StoreVO> getNearVOs(ArrayList<StoreVO> vos, double lat1 , double lng1, double radius ){
        ArrayList<StoreVO> arrayList = new ArrayList<>();

        for ( StoreVO vo : vos) {
            double distance;

            Location locationA = new Location("point A");
            locationA.setLatitude(lat1);
            locationA.setLongitude(lng1);

            Location locationB = new Location("point B");
            locationB.setLatitude(vo.getLat());
            locationB.setLongitude(vo.getLng());

            distance = locationA.distanceTo(locationB);

            if ( distance < radius ){
                arrayList.add(vo);
            }
        }

        return arrayList;
    }
}
