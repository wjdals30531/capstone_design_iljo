package kr.ac.goodpriceshop.capstone_design.goodpriceshop.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import kr.ac.goodpriceshop.capstone_design.goodpriceshop.R;
import kr.ac.goodpriceshop.capstone_design.goodpriceshop.listview.StoreVO;

/**
 * Created by Osy on 2017-11-03.
 */

public class InfoFragment extends Fragment {
    private StoreVO storeVO;

    public static InfoFragment newFragment(StoreVO storeVO){
        InfoFragment INSTANCE = new InfoFragment();
        INSTANCE.storeVO = storeVO;
        return INSTANCE;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info, container, false);

        TextView address = (TextView)view.findViewById(R.id.info_address);
        TextView telephone = (TextView)view.findViewById(R.id.info_telephone);
        TextView office_hours = (TextView)view.findViewById(R.id.info_office_hours);
        TextView closed_day = (TextView)view.findViewById(R.id.info_closed_day);
        Button Call = (Button)view.findViewById(R.id.info_call);

        address.setText(storeVO.getAddress());
        telephone.setText(storeVO.getTelephone());
        office_hours.setText(storeVO.getOffice_hours().replace(", ","\n"));
        closed_day.setText(storeVO.getClosed_day());

        Call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+storeVO.getTelephone()));
                startActivity(intent);
            }
        });
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        TextView info_text  = view.findViewById(R.id.info_text);
    }
}