package kr.ac.goodpriceshop.capstone_design.goodpriceshop.util;

import java.util.ArrayList;

/**
 * Created by Osy on 2017-11-23.
 */

public class XmlUtil {
    private String xmlStr;
    private String objectTagNameStart;
    private String objectTagNameEnd;
    private int tagNameSize;

    ArrayList<String > xmlObjects;

    public XmlUtil(String xmlStr){
        this.xmlStr = xmlStr;
        xmlObjects = new ArrayList<>();
    }

    public void setObjectTagName(String tagName){
        tagNameSize = tagName.length() + 2;

        objectTagNameStart = "<"+tagName+">";
        objectTagNameEnd = "</"+tagName+">";

        start();
    }

    private void start(){
        int startPosition = 0;
        int endPosition = 0;
        while (true){
            startPosition = xmlStr.indexOf( objectTagNameStart, endPosition);
            endPosition = xmlStr.indexOf( objectTagNameEnd, startPosition);

            if (startPosition == -1){
                return;
            }

            String s = xmlStr.substring(startPosition + tagNameSize ,endPosition);
            xmlObjects.add(s);
        }

    }

    public ArrayList<String> getXmlObjects() {
        return xmlObjects;
    }


}
