package kr.ac.goodpriceshop.capstone_design.goodpriceshop.listview;

import java.io.Serializable;

/**
 * Created by Osy on 2017-11-21.
 */

public class StoreVO implements Serializable{
    private String name;
    private String address;
    private String category;
    private String goodPrice;
    private String location;
    private String telephone;
    private String img;
    private String office_hours;
    private String closed_day;
    private double lat = 0;
    private double lng = 0;



    public String getName() {
        if (name == null){
            name="";
        }
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        if (address == null){
            address="";
        }
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCategory() {
        if (category == null){
            category="";
        }
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getGoodPrice() {
        if (goodPrice == null){
            goodPrice="";
        }
        return goodPrice;
    }

    public void setGoodPrice(String goodPrice) {
        this.goodPrice = goodPrice;
    }

    public String getLocation() {
        if (location == null){
            location="";
        }
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTelephone() {
        if (telephone == null){
            telephone="";
        }
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getOffice_hours() {
        return office_hours;
    }

    public void setOffice_hours(String office_hours) {
        this.office_hours = office_hours;
    }

    public String getClosed_day() {
        return closed_day;
    }

    public void setClosed_day(String closed_day) {
        this.closed_day = closed_day;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }


    public static class sampleBuilder{
        private String title;
        private String address;

        public sampleBuilder setTitle(String title) {
            this.title = title;
            return this;
        }

        public sampleBuilder setAddress(String address) {
            this.address = address;
            return this;
        }

        public StoreVO build(){
            StoreVO storeVO = new StoreVO();
            storeVO.setName( title);
            storeVO.setAddress( address);

            return storeVO;
        }
    }
}
