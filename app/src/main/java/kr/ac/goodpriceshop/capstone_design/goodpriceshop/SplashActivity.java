package kr.ac.goodpriceshop.capstone_design.goodpriceshop;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

/**
 * Created by Osy on 2017-12-16.
 */

public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Handler hd = new Handler();
        hd.postDelayed(new splashHandler() , 2000); // 3초 후에 hd Handler 실행
    }

    private class splashHandler implements Runnable{
        public void run() {
            startActivity(new Intent(getApplication(), MainActivity.class));
            finish();
        }
    }
}
