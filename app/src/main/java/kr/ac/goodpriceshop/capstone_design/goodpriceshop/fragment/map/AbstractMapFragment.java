package kr.ac.goodpriceshop.capstone_design.goodpriceshop.fragment.map;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;

/**
 * Created by Osy on 2017-12-10.
 */

public abstract class AbstractMapFragment extends Fragment implements OnMapReadyCallback {
    protected abstract void mapSetting(GoogleMap map);

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(final GoogleMap map) {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            TedPermission.with(getActivity())
                    .setPermissionListener(new PermissionListener() {
                        @Override
                        public void onPermissionGranted() {
                            mapSetting(map);
                            //현위치 볼수 있는 버튼 생김 !
                            map.setMyLocationEnabled(true);
                        }

                        @Override
                        public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                            mapSetting(map);
                        }
                    })
                    .setDeniedMessage("위치 권한을 허가해주시지 않으시면 현재위치를 볼 수 없습니다.\n\n 권한을 바꾸시려면 [설정] > [권한]")
                    .setPermissions(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                    .check();
        }
        else {
            mapSetting(map);
            map.setMyLocationEnabled(true);
        }
    }
}
