package kr.ac.goodpriceshop.capstone_design.goodpriceshop;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;

import kr.ac.goodpriceshop.capstone_design.goodpriceshop.listview.NearShopListActivity;
import kr.ac.goodpriceshop.capstone_design.goodpriceshop.listview.ShowListActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        context = this;

        Button korea = (Button)findViewById(R.id.korea);
        Button japan = (Button)findViewById(R.id.japan);
        Button china = (Button)findViewById(R.id.china);
        Button europe = (Button)findViewById(R.id.europe);
        Button extraFood = (Button)findViewById(R.id.extra);
        Button aroundShop = (Button)findViewById(R.id.aroundshop);
        Button shower = (Button)findViewById(R.id.shower);
        Button hotel = (Button)findViewById(R.id.hotel);
        Button hairSalon = (Button)findViewById(R.id.hairsalon);
        Button clothsWash = (Button)findViewById(R.id.clothwash);
        Button extraService = (Button)findViewById(R.id.extra2);
        Button aroundShop2 = (Button)findViewById(R.id.aroundshop2);

        korea.setOnClickListener(this);
        japan.setOnClickListener(this);
        china.setOnClickListener(this);
        europe.setOnClickListener(this);
        extraFood.setOnClickListener(this);
        aroundShop.setOnClickListener(this);

        shower.setOnClickListener(this);
        hotel.setOnClickListener(this);
        hairSalon.setOnClickListener(this);
        clothsWash.setOnClickListener(this);
        extraService.setOnClickListener(this);
        aroundShop2.setOnClickListener(this);
    }

    private Intent intent;
    @Override
    public void onClick(final View v) {
        if (v.getId() == R.id.aroundshop || v.getId() == R.id.aroundshop2){
            intent = new Intent(context, NearShopListActivity.class);

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                TedPermission.with(this)
                        .setPermissionListener(new PermissionListener() {
                            @Override
                            public void onPermissionGranted() {
                                switch (v.getId()){
                                    case R.id.aroundshop :
                                        intent.putExtra("type","음식업");
                                        break;
                                    case R.id.aroundshop2 :
                                        intent.putExtra("type","서비스업");
                                        break;
                                }
                                startActivity(intent);
                            }

                            @Override
                            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                            }
                        })
                        .setDeniedMessage("위치 권한을 허가해주시지 않으시면 현재위치를 볼 수 없습니다.\n\n 권한을 바꾸시려면 [설정] > [권한]")
                        .setPermissions(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                        .check();
            }
            else {
                switch (v.getId()){
                    case R.id.aroundshop :
                        intent.putExtra("type","음식업");
                        break;
                    case R.id.aroundshop2 :
                        intent.putExtra("type","서비스업");
                        break;
                }
                startActivity(intent);
            }


        }
        else {
            intent = new Intent(context, ShowListActivity.class);
            switch (v.getId()){
                case R.id.korea :
                    intent.putExtra("type","음식업");
                    intent.putExtra("category","한식");
                    break;
                case R.id.japan :
                    intent.putExtra("type","음식업");
                    intent.putExtra("category","일식");
                    break;
                case R.id.china :
                    intent.putExtra("type","음식업");
                    intent.putExtra("category","중식");
                    break;
                case R.id.europe :
                    intent.putExtra("type","음식업");
                    intent.putExtra("category","양식");
                    break;
                case R.id.extra :
                    intent.putExtra("type","음식업");
                    intent.putExtra("category","기타양식");
                    break;

                case R.id.shower :
                    intent.putExtra("type","서비스업");
                    intent.putExtra("category","목욕업");
                    break;
                case R.id.hotel :
                    intent.putExtra("type","서비스업");
                    intent.putExtra("category","숙박업");
                    break;
                case R.id.clothwash :
                    intent.putExtra("type","서비스업");
                    intent.putExtra("category","세탁업");
                    break;
                case R.id.hairsalon :
                    intent.putExtra("type","서비스업");
                    intent.putExtra("category","이미용업");
                    break;
                case R.id.extra2 :
                    intent.putExtra("type","서비스업");
                    intent.putExtra("category","기타서비스");
                    break;
            }
            startActivity(intent);
        }
    }
}
