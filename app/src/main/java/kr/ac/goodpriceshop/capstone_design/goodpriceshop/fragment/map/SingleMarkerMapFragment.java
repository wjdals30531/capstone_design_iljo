package kr.ac.goodpriceshop.capstone_design.goodpriceshop.fragment.map;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;

import kr.ac.goodpriceshop.capstone_design.goodpriceshop.R;
import kr.ac.goodpriceshop.capstone_design.goodpriceshop.listview.StoreVO;

/**
 * Created by Osy on 2017-11-03.
 */

public class SingleMarkerMapFragment extends Fragment implements OnMapReadyCallback {
    private StoreVO vo;

    public static SingleMarkerMapFragment newFragment(StoreVO vo) {
        SingleMarkerMapFragment INSTANCE = new SingleMarkerMapFragment();

        INSTANCE.vo = vo;

        return INSTANCE;
    }

    //context 필요한부분에는 getActivity()사용 하면 됨
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        GoogleMapOptions options = new GoogleMapOptions();
        options.mapType(GoogleMap.MAP_TYPE_NORMAL)
                .compassEnabled(false)
                .rotateGesturesEnabled(false)
                .tiltGesturesEnabled(false);

        MapFragment mapFragment = MapFragment.newInstance(options);
        mapFragment.getMapAsync(this);

        FragmentTransaction fragmentTransaction =
                getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.map_linear, mapFragment);
        fragmentTransaction.commit();
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(final GoogleMap map) {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            TedPermission.with(getActivity())
                    .setPermissionListener(new PermissionListener() {
                        @Override
                        public void onPermissionGranted() {
                            mapSetting(map);
                            //현위치 볼수 있는 버튼 생김 !
                            map.setMyLocationEnabled(true);
                        }

                        @Override
                        public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                            mapSetting(map);
                        }
                    })
                    .setDeniedMessage("위치 권한을 허가해주시지 않으시면 현재위치를 볼 수 없습니다.\n\n 권한을 바꾸시려면 [설정] > [권한]")
                    .setPermissions(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                    .check();
        }
        else {
            mapSetting(map);
            map.setMyLocationEnabled(true);
        }
    }

    public void mapSetting(GoogleMap map){
        LatLng space = new LatLng(vo.getLat(), vo.getLng());

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(space);
        markerOptions.title(vo.getName());
        markerOptions.snippet(vo.getAddress());

        map.addMarker(markerOptions);
        map.moveCamera(CameraUpdateFactory.newLatLng(space));
        map.animateCamera(CameraUpdateFactory.zoomTo(15));
    }
}
