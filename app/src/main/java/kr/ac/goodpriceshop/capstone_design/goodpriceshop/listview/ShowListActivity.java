package kr.ac.goodpriceshop.capstone_design.goodpriceshop.listview;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import kr.ac.goodpriceshop.capstone_design.goodpriceshop.CommonData;
import kr.ac.goodpriceshop.capstone_design.goodpriceshop.IntroduceActivity;
import kr.ac.goodpriceshop.capstone_design.goodpriceshop.R;
import kr.ac.goodpriceshop.capstone_design.goodpriceshop.util.DataHelper;

/**
 * Created by Osy on 2017-12-08.
 */

public class ShowListActivity extends Activity{
    private Provider provider;
    private Context context;
    private ArrayList<StoreVO> categoryVOs;
    private MyAdapter myAdapter;

    private String category;
    private LinearLayout noData;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list);
        context = this;
        category = getIntent().getStringExtra("category");

        TextView categoryView = findViewById(R.id.category);
        categoryView.setText(category);
        initProvider();

        noData = findViewById(R.id.no_data);
        if (provider.getCount() != 0){
            noData.setVisibility(View.GONE);
        }

        RecyclerView recyclerView = findViewById( R.id.recycler_view);
        myAdapter = new MyAdapter( provider, itemClickListener);

        recyclerView.setAdapter( myAdapter );
        DividerItemDecoration dividerItemDecoration =
                new DividerItemDecoration(this, LinearLayoutManager.VERTICAL);

        dividerItemDecoration.setDrawable( this.getResources().getDrawable(R.drawable.list_divider));

        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setLayoutManager( new LinearLayoutManager( this ));
    }

    private void initProvider(){
        ArrayList<StoreVO> storeVOs;

        String type = getIntent().getStringExtra("type");

        if (type.equals("서비스업")){
            storeVOs = ((CommonData)getApplication()).getServiceVOs();
        }
        else {
            storeVOs = ((CommonData)getApplication()).getFoodVOs();
        }

        categoryVOs = DataHelper.getVOsOfCategory( storeVOs, category);
        provider = new Provider( categoryVOs );
    }

    private View.OnClickListener itemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int position = (Integer)view.getTag();
            StoreVO storeVO = categoryVOs.get(position);

            Intent intent = new Intent( context, IntroduceActivity.class);
            intent.putExtra( "storeVO", storeVO);
            startActivity(intent);
        }
    };
}
