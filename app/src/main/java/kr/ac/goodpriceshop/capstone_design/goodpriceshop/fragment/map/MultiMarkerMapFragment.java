package kr.ac.goodpriceshop.capstone_design.goodpriceshop.fragment.map;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import kr.ac.goodpriceshop.capstone_design.goodpriceshop.R;
import kr.ac.goodpriceshop.capstone_design.goodpriceshop.listview.StoreVO;

/**
 * Created by Osy on 2017-12-10.
 */

public class MultiMarkerMapFragment extends AbstractMapFragment {
    private ArrayList<StoreVO> vos;
    private LatLng myLatLng;
    private GoogleMap map;
    private int i = 0;

    public static MultiMarkerMapFragment newFragment(ArrayList<StoreVO> vos, LatLng myLatLng) {
        MultiMarkerMapFragment INSTANCE = new MultiMarkerMapFragment();
        INSTANCE.vos = vos;
        INSTANCE.myLatLng = myLatLng;

        return INSTANCE;
    }

    public void resetVOs(ArrayList<StoreVO> vos){
        this.vos = vos;
        markerSetting();
    }
    public void resetMyLatLng(LatLng myLatLng){
        this.myLatLng = myLatLng;
        if (i == 0){
            map.moveCamera( CameraUpdateFactory.newLatLng( myLatLng) );
            i++;
        }


    }

    //context 필요한부분에는 getActivity()사용 하면 됨
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        GoogleMapOptions options = new GoogleMapOptions();
        options.mapType(GoogleMap.MAP_TYPE_NORMAL)
                .compassEnabled(false)
                .rotateGesturesEnabled(false)
                .tiltGesturesEnabled(false);

        MapFragment mapFragment = MapFragment.newInstance(options);
        mapFragment.getMapAsync(this);

        FragmentTransaction fragmentTransaction =
                getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.map_linear, mapFragment);
        fragmentTransaction.commit();
    }

    @Override
    protected void mapSetting(GoogleMap map){
        this.map = map;

        markerSetting();
        map.moveCamera( CameraUpdateFactory.newLatLng( myLatLng) );
        map.animateCamera( CameraUpdateFactory.zoomTo(11) );
    }

    private void markerSetting(){
        map.clear();
        for (StoreVO vo: vos) {
            LatLng space = new LatLng(vo.getLat(), vo.getLng());

            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(space);
            markerOptions.title(vo.getName());
            markerOptions.snippet(vo.getAddress());

            map.addMarker(markerOptions);
        }
    }
}
