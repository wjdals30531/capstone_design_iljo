package kr.ac.goodpriceshop.capstone_design.goodpriceshop;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import kr.ac.goodpriceshop.capstone_design.goodpriceshop.fragment.InfoFragment;
import kr.ac.goodpriceshop.capstone_design.goodpriceshop.fragment.MenuFragment;
import kr.ac.goodpriceshop.capstone_design.goodpriceshop.fragment.map.SingleMarkerMapFragment;
import kr.ac.goodpriceshop.capstone_design.goodpriceshop.listview.StoreVO;
import kr.ac.goodpriceshop.capstone_design.goodpriceshop.util.ImageUtil;

/**
 * Created by Osy on 2017-11-03.
 */

public class IntroduceActivity extends Activity {
    private StoreVO storeVO;

    ImageView shopImage;
    Bitmap bitmap;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_introduce);

        Intent intent = getIntent();
        storeVO = (StoreVO)intent.getSerializableExtra("storeVO");

        initFragment();
        initTabHost();

        shopImage = findViewById(R.id.shop_img);
        imageSetting();

        TextView title = findViewById(R.id.title);
        TextView category = findViewById(R.id.category);

        title.setText(storeVO.getName());
        category.setText(storeVO.getCategory());

    }
    private void initTabHost(){
        TabHost tabHost1 = (TabHost) findViewById(R.id.tabHost1) ;
        tabHost1.setup() ;
        tabHost1.setBackground(getResources().getDrawable(R.drawable.shape));

        // 첫 번째 Tab. (탭 표시 텍스트:"TAB 1"), (페이지 뷰:"content1")
        TabHost.TabSpec ts1 = tabHost1.newTabSpec("Tab Spec 1");
        ts1.setContent(R.id.content1);
        ts1.setIndicator("상세정보");
        tabHost1.addTab(ts1);

        // 두 번째 Tab. (탭 표시 텍스트:"TAB 2"), (페이지 뷰:"content2")
        TabHost.TabSpec ts2 = tabHost1.newTabSpec("Tab Spec 2");
        ts2.setContent(R.id.content2);
        ts2.setIndicator("메뉴");
        tabHost1.addTab(ts2);

        // 세 번째 Tab. (탭 표시 텍스트:"TAB 3"), (페이지 뷰:"content3")
        TabHost.TabSpec ts3 = tabHost1.newTabSpec("Tab Spec 3");
        ts3.setContent(R.id.content3);
        ts3.setIndicator("지도");
        tabHost1.addTab(ts3);
    }

    private void imageSetting(){
        Thread thread = new Thread(){
            @Override
            public void run() {
                try {
                    URL url = new URL(storeVO.getImg());

                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setDoInput(true);
                    conn.connect();

                    InputStream is = conn.getInputStream();

                    final BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;  //이미지의 크기를 먼저 불러오기위해
                    BitmapFactory.decodeStream(is, null, options);

                    options.inSampleSize = ImageUtil.calculateInSampleSize(options, 640, 480);
                    options.inJustDecodeBounds = false;
                    bitmap = BitmapFactory.decodeStream( url.openStream(), null, options);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };

        thread.start();
        try {
            thread.join();
            shopImage.setImageBitmap(bitmap);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void initFragment(){
        Fragment cur_fragment = InfoFragment.newFragment(storeVO);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content1, cur_fragment);
        fragmentTransaction.commit();

        Fragment menuFragment = MenuFragment.newFragment(storeVO);
        FragmentTransaction fragmentTransaction2 = fragmentManager.beginTransaction();
        fragmentTransaction2.replace(R.id.content2, menuFragment);
        fragmentTransaction2.commit();

        Fragment mapFragment = SingleMarkerMapFragment.newFragment(storeVO);
        FragmentTransaction fragmentTransaction3 = fragmentManager.beginTransaction();
        fragmentTransaction3.replace(R.id.content3, mapFragment);
        fragmentTransaction3.commit();
    }
}
