package kr.ac.goodpriceshop.capstone_design.goodpriceshop.listview;

import java.util.ArrayList;

/**
 * Created by Osy on 2017-11-21.
 */

public class Provider {
    private int count;
    private ArrayList<StoreVO> VOs;

    public Provider( final ArrayList<StoreVO> VOs){
        this.VOs = VOs;
        count = VOs.size();
    }

    public int getCount(){
        return count;
    }

    public String getName(int position){
        return VOs.get(position).getName();
    }
    public String getAddress(int position) {
        return VOs.get(position).getAddress();
    }
    public String getCategory(int position){
        return VOs.get(position).getCategory();
    }
    public String getTelephone(int position){
        return VOs.get(position).getTelephone();
    }
    public String getLocation(int position){
        return VOs.get(position).getLocation();
    }
    public String getGoodPrice(int position){
        return VOs.get(position).getGoodPrice();
    }

    public void addStoreVOs(ArrayList<StoreVO> storeVOs){
        VOs.addAll(storeVOs);
        count = VOs.size();
    }
    public ArrayList<StoreVO> getVOs(){
        return VOs;
    }
}
