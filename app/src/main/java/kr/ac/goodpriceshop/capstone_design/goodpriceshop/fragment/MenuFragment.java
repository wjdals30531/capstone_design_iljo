package kr.ac.goodpriceshop.capstone_design.goodpriceshop.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import kr.ac.goodpriceshop.capstone_design.goodpriceshop.R;
import kr.ac.goodpriceshop.capstone_design.goodpriceshop.listview.StoreVO;

/**
 * Created by Osy on 2017-11-03.
 */

public class MenuFragment extends Fragment {
    private StoreVO storeVO;

    public static MenuFragment newFragment(StoreVO storeVO){
        MenuFragment INSTANCE = new MenuFragment();
        INSTANCE.storeVO = storeVO;

        return INSTANCE;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu, container, false);

        TextView textView = (TextView)view.findViewById(R.id.menu);
        String tmp = storeVO.getGoodPrice().replace(" ","");

        String menu = "■   " + tmp.replace("/", "\n\n■   ");

        textView.setText(menu);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        TextView tv  = view.findViewById(R.id.text1);

    }
}
