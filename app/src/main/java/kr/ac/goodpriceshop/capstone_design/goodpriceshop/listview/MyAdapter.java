package kr.ac.goodpriceshop.capstone_design.goodpriceshop.listview;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import kr.ac.goodpriceshop.capstone_design.goodpriceshop.R;

/**
 * Created by Osy on 2017-11-21.
 */

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private Provider provider;
    private View.OnClickListener itemClickListener;

    public MyAdapter (Provider provider , View.OnClickListener itemClickListener){
        this.provider = provider;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate( R.layout.recycler_view, parent, false);
        v.setOnClickListener(itemClickListener);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.itemView.setTag(position);
        holder.title.setText( provider.getName(position));
        holder.address.setText( provider.getAddress(position));
    }

    @Override
    public int getItemCount() {
        return provider.getCount();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView address;

        protected MyViewHolder(View v) {
            super(v);

            title = v.findViewById(R.id.name);
            address = v.findViewById(R.id.address);
        }
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
        notifyDataSetChanged();
    }
}
