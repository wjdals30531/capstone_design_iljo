package kr.ac.goodpriceshop.capstone_design.goodpriceshop.util;

import android.content.res.AssetManager;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import kr.ac.goodpriceshop.capstone_design.goodpriceshop.listview.StoreVO;

/**
 * Created by Osy on 2017-12-08.
 */

public class JsonUtil {
    private final String fileName = "test.json";
    private AssetManager assetManager;
    private ArrayList<StoreVO> storeVOs;

    public JsonUtil(AssetManager assetManager){
        this.assetManager = assetManager;
        storeVOs = readJson();
    }

    private ArrayList<StoreVO> readJson(){
        String response;
        JSONArray responseJSON;
        ArrayList<StoreVO> storeVOs = null;

        try {
            InputStream is = assetManager.open( fileName);

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream ( );
            byte [ ] byteBuffer = new byte [ 1024 ];
            byte [ ] byteData = null;
            int nLength = 0;

            while ( ( nLength = is.read ( byteBuffer , 0 , byteBuffer.length ) ) != -1 )
            {
                byteArrayOutputStream.write ( byteBuffer , 0 , nLength );
            }

            byteData = byteArrayOutputStream.toByteArray ( );

            Log.e(this.toString(), "readJson: 설마 여기서?");

            if( byteData.length <= 0 )
            {
                Log.e(this.toString(), "readJson: 리턴되버리나?");

                return null;
            }
            Log.e(this.toString(), "readJson: 리턴안되는뎅..");
            response = new String ( byteData );

            responseJSON = new JSONArray ( response );

            storeVOs = getJsonParse( responseJSON );

            byteArrayOutputStream.close ( );
            is.close ( );

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return storeVOs;
    }

    private ArrayList <StoreVO> getJsonParse(JSONArray jsonArray ){
        Log.e(this.toString(), "jsonArray.length: " + jsonArray.length() );
        if ( jsonArray == null ) {
            return null;
        }
        ArrayList < StoreVO > arrayList = new ArrayList <> ( );

        Log.e(this.toString(), "jsonArray.length: " + jsonArray.length() );
        for ( int i = 0 ; i < jsonArray.length ( ) ; i++ )
        {
            JSONObject jsonObject;
            StoreVO storeVO = new StoreVO ( );
            try
            {
                jsonObject = jsonArray.getJSONObject ( i );

                storeVO.setLat ( jsonObject.has ( "lat" ) ? Double.valueOf(jsonObject.getString ( "lat" )) : 0 );
                storeVO.setLng ( jsonObject.has ( "lng" ) ? Double.valueOf(jsonObject.getString ( "lng" )) : 0 );
                storeVO.setImg ( jsonObject.has ( "img" ) ? jsonObject.getString ( "img" ) : null );
                storeVO.setName ( jsonObject.has ( "name" ) ? jsonObject.getString ( "name" ) : null );
                storeVO.setAddress ( jsonObject.has ( "address" ) ? jsonObject.getString ( "address" ) : null );
                storeVO.setCategory ( jsonObject.has ( "category" ) ? jsonObject.getString ( "category" ) : null );
                storeVO.setGoodPrice ( jsonObject.has ( "goods_price" ) ? jsonObject.getString ( "goods_price" ) : null );
                storeVO.setLocation ( jsonObject.has ( "location" ) ? jsonObject.getString ( "location" ) : null );
                storeVO.setTelephone ( jsonObject.has ( "telephone" ) ? jsonObject.getString ( "telephone" ) : null );
                storeVO.setClosed_day ( jsonObject.has ( "closed_day" ) ? jsonObject.getString ( "closed_day" ) : null );
                storeVO.setOffice_hours ( jsonObject.has ( "office_hours" ) ? jsonObject.getString ( "office_hours" ) : null );
            }
            catch ( JSONException e )
            {
                e.printStackTrace();
            }
            catch ( NullPointerException e )
            {
                e.printStackTrace();
            }

            arrayList.add ( storeVO );
        }

        return arrayList;
    }

    public ArrayList<StoreVO> getStoreVOs() {
        return storeVOs;
    }
}
